//
//  ViewController.swift
//  Test
//
//  Created by Jorge Encinas on 9/11/19.
//  Copyright © 2019 Jorge Encinas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    
    @IBOutlet weak var zipCode_TF: UITextField!
    
    @IBAction func askingForWeather(_ sender: UIButton) {
        let obj = WheatherService()
       
        guard let zipCode = Int(zipCode_TF.text ?? "0" ) else {
            return
        }
        print("The weather is: \(obj.getWheather(zip:zipCode))")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    
}


